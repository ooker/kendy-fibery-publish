import "dotenv/config"
import got from "got"
import process from "node:process"
import { extname, basename } from "node:path"
import unified from "unified"
import remarkParse from "remark-parse"
import visit from "unist-util-visit"
import { pipeline as streamPipeline } from "node:stream/promises"
import { createWriteStream } from "node:fs"
import { mkdir, writeFile, readdir } from "node:fs/promises"
import toMarkdown from "mdast-util-to-markdown"

const fiberyAsCms = process.env["FIBERY_AS_CMS"]
const fiberyHost = process.env["FIBERY_HOST"]
const fiberyToken = process.env["FIBERY_TOKEN"]
const fiberyArticleDatabase = process.env["FIBERY_ARTICLE_DATABASE"]
const fiberyArticleSpace = process.env["FIBERY_ARTICLE_SPACE"]

if (!fiberyHost) {
  throw new Error("FIBERY_HOST is not set")
}
if (!fiberyToken) {
  throw new Error("FIBERY_TOKEN is not set")
}
if (!fiberyArticleDatabase) {
  throw new Error("FIBERY_ARTICLE_DATABASE is not set")
}
if (!fiberyArticleSpace) {
  throw new Error("FIBERY_ARTICLE_SPACE is not set")
}

const fiberyUrl = `${fiberyHost}/api/graphql/space/${fiberyArticleSpace}`

const fiberyHeaders = {
  Authorization: `Token ${fiberyToken}`,
}

function getFiberyArticles() {
  return got
    .post(fiberyUrl, {
      headers: fiberyHeaders,
      json: {
        query: `
            query {
              findArticles(published: {is: true}) {
                name
                publicId
                content {
                  md
                }
                creationDate
                date
                description {
                  text
                }
                seoTitle
                seoDescription {
                  text
                }
                slug
                purpose {
                  id
                  name,
                  value
                },
                author {
                  name
                  authorTitle
                  avatars {
                    name
                    secret
                    contentType
                  }
                },
                files {
                  secret
                  name
                  contentType
                }
                tags {
                  name
                }
                category {
                  name
                }
                questions {
                  name
                  answer {
                    text
                  }
               }
              }
            }
            `,
      },
    })
    .json()
}
function downloadImage(url, path) {
  return streamPipeline(
    got.stream(fiberyHost + url, { headers: fiberyHeaders }),
    createWriteStream(path)
  )
}

let label = "read content from fibery"
console.time(label)
const { data, errors } =
  fiberyAsCms === "true"
    ? await getFiberyArticles()
    : { data: { [`find${fiberyArticleDatabase}`]: [] } }
console.log({ fiberyAsCms })
console.log(
  fiberyAsCms ? "fetched article from fibery" : "skipped fetching from fibery"
)
if (errors) {
  throw new Error(`Something went wrong: ${JSON.stringify(errors)}`)
}

function articleTemplate({
  content,
  title,
  description,
  seoTitle,
  seoDescription,
  date,
  author,
  avatar,
  authorTitle,
  category,
  tags,
  purpose,
  featuredImage = ``,
  socialImage = ``,
  questions = [],
}) {
const faq =   questions.length ?
`faq:
  ${questions.map(({name, answer}) => `- question: ${JSON.stringify(name)}
    answer: ${JSON.stringify(answer.text)}`).join('\n  ')}
`: ``;
  return `---
title: "${title}"
description: "${description}"
date: ${JSON.stringify(date)}
author: ${author}
avatar: ${avatar}
authorTitle: ${authorTitle}
category: ${category}
tags: ${JSON.stringify(tags)}
purpose: ${JSON.stringify(purpose)}
featuredImage: ${featuredImage}
socialImage: ${socialImage}
seoTitle: "${seoTitle}"
seoDescription: "${seoDescription}"
${faq}
---
${content}
`
}

async function getFileSourceSlugs() {
  return new Set(
    (await readdir("content/blog", { withFileTypes: true }))
      .filter((dirent) => dirent.isDirectory())
      .map((dirent) => dirent.name)
  )
}

async function validateArticles(articles) {
  const slugs = await getFileSourceSlugs()
  const slugsMap = new Map([])
  for (const article of articles) {
    if (!article.slug) {
      throw new Error(
        `Article ${article.name} with publicId ${article.name} does not have a slug. This article will not be created. Please add a slug to the article in Fibery`
      )
    }
    if (slugs.has(article.slug)) {
      throw new Error(
        `Duplicate ${article.name} with publicId ${article.name} and slug ${article.slug}. This article already exists in the blog file system.`
      )
    }
    if (slugsMap.has(article.slug)) {
      const origin = slugsMap.get(article.slug)
      throw new Error(
        `Duplicate ${article.name} with publicId ${article.name} and slug ${article.slug}. This article already exists in Fibery. Origin: ${origin.name} with publicId ${origin.publicId}. Please remove the duplicate in Fibery.`
      )
    }
    slugsMap.set(article.slug, {
      publicId: article.publicId,
      name: article.name,
    })
  }
}

const articles = data[`find${fiberyArticleDatabase}`]
const assetsDir = `content/fibery-source-assets`
const avatarsCache = new Map()
async function loadAvatar(author) {
  let avatars = author?.avatars
  if (avatars?.length) {
    const avatar = avatars[0]
    const secret = avatar.secret
    const name = avatar.name
    const ext = extname(name)
    const fileName = secret + ext
    const cached = avatarsCache.get(secret)
    if (cached) {
      return fileName
    }
    await downloadImage(`/api/files/${secret}`, assetsDir + "/" + fileName)
    avatarsCache.set(secret, fileName)
    return fileName
  }
  return `fiberylogo.png`
}
await validateArticles(articles)
async function downloadFeaturedAndSocialImage(files, articleDir) {
  let featuredImage
  let socialImage
  for (const { name, secret } of files) {
    const ext = extname(name)
    const nameWithoutExt = basename(name, ext)
    if (nameWithoutExt === "featuredImage" && !featuredImage) {
      featuredImage = secret + ext
      await downloadImage(
        `/api/files/${secret}`,
        articleDir + "/" + featuredImage
      )
    }
    if (nameWithoutExt === "socialImage" && !socialImage) {
      socialImage = secret + ext
      await downloadImage(
        `/api/files/${secret}`,
        articleDir + "/" + featuredImage
      )
    }
  }
  if (!featuredImage && files[0]) {
    const { name, secret } = files[0]
    const ext = extname(name)
    featuredImage = secret + ext
    await downloadImage(
      `/api/files/${secret}`,
      articleDir + "/" + featuredImage
    )
  }
  return {
    featuredImage: featuredImage || socialImage,
    socialImage: socialImage || featuredImage,
  }
}
for (const article of articles) {
  const { md } = article.content
  if (!article.slug) {
    continue
  }
  const articleDir = `content/blog/${article.slug}`
  try {
    await mkdir(articleDir, { recursive: true })
  } catch (err) {
    console.error(err)
  }
  const contentAST = unified().use(remarkParse).parse(md)
  const files = []
  visit(contentAST, "image", async (node) => {
    const url = node.url
    if (url.startsWith("/api/files/")) {
      const urlObject = new URL(url, fiberyHost)
      const ext = extname(node.alt)
      const imageName = urlObject.pathname.replace("/api/files/", "") + ext
      files.push({ url, name: imageName })
      node.url = "./" + imageName
      node.alt = node.title ? node.title : ""
    }
  })
  visit(contentAST, "link", async (node) => {
    const url = node.url
    if (url.startsWith("https://") && url.endsWith(".mp4")) {
      node.type = `html`
      node.value = `<video autoPlay controls loop muted playsInline style={{width: "100%", maxWidth: 700}}>
    <source src="${node.url}" type="video/mp4" />
</video>`
    }
  })
  for (const file of files) {
    await downloadImage(file.url, articleDir + "/" + file.name)
  }
  const { featuredImage, socialImage } = await downloadFeaturedAndSocialImage(
    article.files,
    articleDir
  )
  await writeFile(
    articleDir + "/index.md",
    articleTemplate({
      content: toMarkdown(contentAST, { resourceLink: true }),
      title: article.name,
      tags: article.tags ? article.tags.map(({ name }) => name) : [],
      purpose: article.purpose
        ? article.purpose.map(({ value }) => value)
        : [4],
      author: article.author?.name || "Fibery",
      authorTitle: article.author?.authorTitle || "Mr. Robot",
      avatar: await loadAvatar(article.author),
      category: article.category?.name || "uncategorized",
      description: article.description.text.replaceAll("\n", " "),
      seoDescription: article.seoDescription.text.replaceAll("\n", " ").substring(0, 160),
      seoTitle: article.seoTitle ?? "",
      date: article.date || article.creationDate,
      featuredImage,
      socialImage,
      questions: article.questions,
    })
  )
}
console.timeEnd(label)